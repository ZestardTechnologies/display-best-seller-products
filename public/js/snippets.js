var $zestard_quick_buy = "";
var base_path_quick_buy = "https://zestardshop.com/shopifyapp/quick_buy/public/";
var shop_name = Shopify.shop;
var quick_buy_products, settings, autoplay, color, style, size, products, slider_title, slider_subtitle,display_border,shop_currency,url,name,loop;

//Check if Jquery is Undefined or Not available.
if (typeof jQuery == 'undefined') 
{
	//alert('jQuery is Undefined');	
	//If Undefined or Not available, Then Load	
	(function(){    
		var jscript = document.createElement("script");		
		var flexslider_script = document.createElement("script");		
		var modal_script = document.createElement("script");		
		jscript.src = base_path_quick_buy + "js/zestard_jquery_3.3.1.js";		
		jscript.type = 'text/javascript';
		jscript.async = false;
		
		var el = document.getElementById('quick_buy_slider'),
		elChild = document.createElement('div');
		elChild.id = 'quick_buy_jquery';		
		el.insertBefore(elChild, el.firstChild); 	
		
		document.getElementById('quick_buy_jquery').append(jscript);		
		jscript.onload = function(){			
			$zestard_quick_buy = window.jQuery;				
			//alert('jQuery Loaded');						
			flexslider_script.src = base_path_quick_buy + "js/jquery.flexslider.js";		
			flexslider_script.type = 'text/javascript';
			flexslider_script.async = false;			
			document.getElementById('quick_buy_jquery').append(flexslider_script);
				flexslider_script.onload = function() {
					//alert('flexslider Loaded');		
					modal_script.src = base_path_quick_buy + "js/jquery.modal.min.js";		
					modal_script.type = 'text/javascript';
					modal_script.async = false;									
					document.getElementById('quick_buy_jquery').append(modal_script);	
					modal_script.onload = function() {
						//alert('modal Loaded');		
						quick_buy();				
					};
				};							
			};
		})();
}
else
{
	$zestard_quick_buy = window.jQuery;	
	var flexslider_script = document.createElement("script");		
	var modal_script = document.createElement("script");		
	flexslider_script.src = base_path_quick_buy + "js/jquery.flexslider.js";		
	flexslider_script.type = 'text/javascript';
	flexslider_script.async = false;
	
	var el = document.getElementById('quick_buy_slider'),
	elChild = document.createElement('div');
	elChild.id = 'quick_buy_jquery';		
	el.insertBefore(elChild, el.firstChild); 	
	
	document.getElementById('quick_buy_jquery').append(jscript);		
	flexslider_script.onload = function() {			
		$zestard_quick_buy = window.jQuery;				
		//alert('jQuery Loaded');						
		flexslider_script.src = base_path_quick_buy + "js/jquery.flexslider.js";		
		flexslider_script.type = 'text/javascript';
		flexslider_script.async = false;			
		document.getElementById('quick_buy_jquery').append(flexslider_script);
			flexslider_script.onload = function() {
				//alert('flexslider Loaded');		
				modal_script.src = base_path_quick_buy + "js/jquery.modal.min.js";		
				modal_script.type = 'text/javascript';
				modal_script.async = false;									
				document.getElementById('quick_buy_jquery').append(modal_script);	
				modal_script.onload = function() {
					//alert('modal Loaded');		
					quick_buy();				
				};
			};							
		};
}

function quick_buy()
{	
	$zestard_quick_buy.ajax({
		url:base_path_quick_buy + "get-details",
		data:{shop:Shopify.shop},
		success:function(result){			
		}
	});
	
    $zestard_quick_buy.ajax({
        url: base_path_quick_buy + "get-products",
        data: {
            shop_name: shop_name
        },
        crossDomain: true,
        async: false,
        success: function(result) {			
			if(result)
			{
				quick_buy_products = JSON.parse(result);
			}			
        }
    });
	if(quick_buy_products)
	{
		$zestard_quick_buy.ajax({
			url: base_path_quick_buy + "get-settings",
			data: {
				shop_name: shop_name
			},
			crossDomain: true,
			success: function(result) {
				settings = JSON.parse(result);
				if (settings[0].app_status == 1) {					
					color = settings[0].border_color;
					display_border = settings[0].display_border;
					style = settings[0].border_style;
					size = settings[0].border_size;
					products = settings[0].number_of_products;
					autoplay = settings[0].autoplay_slider;
					slider_title = settings[0].slider_title;
					slider_subtitle = settings[0].slider_subtitle;
					shop_currency = settings[0].shop_currency;				
					loop = settings[0].loop;
					product_click = settings[0].product_click;				
					length = quick_buy_products.length;					
					if(length > 0)
					{
						if (autoplay == 1) 
						{
							autoplay = true;
						} 
						else 
						{
							autoplay = false;
						}						
						if (loop == 1) 
						{
							loop = true;
						} 
						else 
						{
							loop = false;
						}
						if(display_border == 1)
						{
							$zestard_quick_buy(".quick_buy").css("border", size + " " + style + " "+ color);
						}
						$zestard_quick_buy(".preview_title").html(slider_title);
						$zestard_quick_buy(".preview_subtitle").html(slider_subtitle);
										
						if(product_click == 1)
						{
							for (var i = 0; i < length; i++) 
							{
								product_id = quick_buy_products[i].product_id;
								name = quick_buy_products[i].product_name;					
								variants = JSON.parse(quick_buy_products[i].variants);
								variants_select = "<select class='product_variants' id='" +
								product_id + "' ><option value=''>Select Variant</option>";
								$zestard_quick_buy.each(variants, function(key, value){
									variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
								});
								variants_select = variants_select + "</select>";
																
								/*
								if(name.length > 10) 
									name = name.substring(0,10) + "....";
								else
									name = name.substring(0,10); 
								*/
								
								url = shop_name+'/products/'+quick_buy_products[i].product_handle;								
								$zestard_quick_buy(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + quick_buy_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> <b>"+shop_currency+" "+ quick_buy_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id +"' class='product_add btn btn-info' >Add to Cart</a></li>");
								//$zestard_quick_buy(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + quick_buy_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> </li>");
							}	
							product_add();
						}
						else
						{							
							for (var i = 0; i < length; i++) 
							{
								product_id = quick_buy_products[i].product_id;
								name 	 = quick_buy_products[i].product_name;
								variants = JSON.parse(quick_buy_products[i].variants);
								variants_select = "<select class='product_variants' id='" +product_id + "' ><option value=''>Select Variant</option>";
								$zestard_quick_buy.each(variants, function(key, value){
									variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
								});
								variants_select = variants_select + "</select>";
							
								/*
								if(name.length > 10) 
									name = name.substring(0,10) + "....";
								else
									name = name.substring(0,10);								 
								*/
								
								$zestard_quick_buy(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-2' data-id='"+ product_id + "' src='" + quick_buy_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-2' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + " <br> " + shop_currency + " " + quick_buy_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id  +"' class='product_add btn btn-info'>Add to Cart</a></li>");
								//$zestard_quick_buy(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-2' data-id='"+ product_id + "' src='" + quick_buy_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-2' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + "</li>");
							}
							show_popup();
							product_add();
							modal_product_add();
						}
						$zestard_quick_buy(".flexslider .slides img").css("height", "160px");
						$zestard_quick_buy(".flexslider .slides img").css("cursor", "pointer");
						if (length > 0) {
							$zestard_quick_buy('.quick_buy').flexslider({
								animation: "slide",
								animationLoop: loop,
								slideshow: autoplay,
								itemWidth: 410,
								itemMargin: 25,
								minItems: products,
								maxItems: products,
							});
						}
					$zestard_quick_buy(".flex-next").html("<img height=20 width=20 src='" + base_path_quick_buy + "image/right.png' />");
					$zestard_quick_buy(".flex-prev").html("<img height=20 width=20 src='" + base_path_quick_buy + "image/left.png' />");									
					$zestard_quick_buy(".flex-next").css("right","0");
					$zestard_quick_buy(".flex-next").css("position","absolute");
					$zestard_quick_buy(".flex-prev").css("left","0");
					$zestard_quick_buy(".flex-prev").css("position","absolute");
					/* 
						$zestard_quick_buy(".flex-next").html("<i class='glyphicon glyphicon-chevron-right'></i>");
						$zestard_quick_buy(".flex-prev").html("<i class='glyphicon glyphicon-chevron-left'></i>");
						$zestard_quick_buy(".flex-next").html("<i class='fa fa-chevron-circle-left'></i>");
						$zestard_quick_buy(".flex-prev").html("<i class='fa fa-chevron-circle-right'></i>"); 
						$zestard_quick_buy(".flex-next").html("<p><span class='glyphicon glyphicon-chevron-right'></span></p>");
						$zestard_quick_buy(".flex-prev").html("<p><span class='glyphicon glyphicon-chevron-left'></span></p>");					
					*/	
					}			
				} 
				else 
				{
					$zestard_quick_buy("#best_seller_slider").remove();
				}
			}
		});
	}
}
function product_add()
{			
	$zestard_quick_buy(".product_add").click(function(){							
		var id = $zestard_quick_buy(this).attr("data-id");
		product_id = $zestard_quick_buy("#"+id).val();				
		if (product_id == '' )
		{
			alert('Please Select Variant');
		}
		else
		{
			$zestard_quick_buy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
				location.reload();					
			});	
		}			
	});
}

function modal_product_add()
{					
	$zestard_quick_buy(".product_add_modal").click(function(){							
		var id = $zestard_quick_buy(this).attr("data-id");
		product_id = $zestard_quick_buy("#select_variant_div #"+id).val();			
		if (product_id == '' )
		{
			alert('Please Select Variant');
		}
		else
		{
			$zestard_quick_buy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
				location.reload();					
			});	
		}			
	});
}

function show_popup()
{	
	var targeted_popup_class;		
	//Opening Modal
	$zestard_quick_buy('.product_click').on('click', function(e){				
		$zestard_quick_buy("#quick_buy_popup_modal").css("display","block");
		$zestard_quick_buy('html, body').animate({
			scrollTop: $zestard_quick_buy("#quick_buy_content").offset().top - 100 //+ $zestard_quick_buy("#quick_buy_content").height() / 2
		}, 2000);
		
		targeted_popup_class = $zestard_quick_buy(this).attr('data-popup-open');					
		$zestard_quick_buy('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);							
		e.preventDefault();
	});
	//Closing Modal
	$zestard_quick_buy('[data-popup-close]').on('click', function(e){
		targeted_popup_class = $zestard_quick_buy(this).attr('data-popup-close');			
		$zestard_quick_buy('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
	});		
	
	var id='',index;				
	$zestard_quick_buy(".product_click").click(function(){
		id = $zestard_quick_buy(this).attr("data-id");	
		length = quick_buy_products.length;				
		for (var i = 0; i < length; i++) 
		{
			if(id == quick_buy_products[i].product_id)
			{
				index = i;
			}
		}							
		price 	=	shop_currency+" "+quick_buy_products[index].product_price;
		img_src =	quick_buy_products[index].product_image;
		name 	=	quick_buy_products[index].product_name;
		desc 	=	quick_buy_products[index].product_description;
		variant_id 	=	quick_buy_products[index].product_variant_id;	
		variants = JSON.parse(quick_buy_products[index].variants);
		variants_select = "<select id='" + id + "'><option value=''>Select Variant</option>";
		$zestard_quick_buy.each(variants, function(key, value){
			variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
		});
		variants_select = variants_select + "</select>";	
		add_to_cart = "<a data-id='"+ id +"' class='product_add_modal btn btn-info' style='background:#7796a8;'>Add to Cart</a>";			
		$zestard_quick_buy("#quick_buy_image").attr("src",img_src);			
		$zestard_quick_buy("#product_name").html(name);
		$zestard_quick_buy("#product_price").html(price);
		$zestard_quick_buy("#product_desc").html(desc);			
		$zestard_quick_buy("#add_to_cart_button").html(add_to_cart);
		$zestard_quick_buy("#select_variant_div").html(variants_select);
		modal_product_add();
	});				
}