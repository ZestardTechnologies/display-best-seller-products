var base_path_most_view = "https://zestardshop.com/shopifyapp/quick_buy/public/";
var shop_name = Shopify.shop, products_array, settings;
	if (typeof jQuery == 'undefined') 
	{	
		//alert('jQuery is Undefined');	
		//If Undefined or Not available, Then Load				
		var jscript = document.createElement("script");
		var flexslider_script = document.createElement("script");
		var modal_script = document.createElement("script");
		jscript.src = base_path_most_view + "js/zestard_jquery_3.3.1.js";		
		jscript.type = 'text/javascript';
		jscript.async = false;
		
		var el = document.getElementById('most_view_box_jquery'),
		elChild = document.createElement('div');
		elChild.id = 'jquery_box';		
		el.insertBefore(elChild, el.firstChild); 	
		document.getElementById('jquery_box').append(jscript);
		jscript.onload = function() {						
			$zestard_most_view = jQuery;
			flexslider_script.src = base_path_most_view + "js/jquery.flexslider.js";		
			flexslider_script.type = 'text/javascript';
			flexslider_script.async = false;			
			document.getElementById('jquery_box').append(flexslider_script);
			flexslider_script.onload = function() {					
				modal_script.src = base_path_most_view + "js/jquery.modal.min.js";		
				modal_script.type = 'text/javascript';
				modal_script.async = false;									
				document.getElementById('jquery_box').append(modal_script);	
				modal_script.onload = function() {	
					most_view();
				};			
			};
		};	
	}
	else
	{
		$zestard_most_view = jQuery;						
		most_view();
	}
	function most_view()
	{	
		if(product_page == 1)
		{		
			$zestard_most_view.ajax({
				url: base_path_most_view + "view-product",
				data:{product_id: product_id },
				async:false,
				crossDomain:true,
				sucess:function(result)
				{
					alert(result);
				}
			});
		}
		$zestard_most_view.ajax({
			url: base_path_most_view + "get-settings",
			data: {
				shop_name: shop_name
			},
			crossDomain: true,
			async:false,	
			success: function(result) {
				settings = JSON.parse(result);				
			}
		});	
		$zestard_most_view.ajax({
			url: base_path_most_view + 'get-most-product', 				
			async:false,
			data: { shop_name : shop_name },	
			success: function(result)
			{
				products_array = result;	
			}
		});			
		
		console.log(products_array);
		most_viewed_products = $.parseJSON(products_array); 												
		if (settings[0].app_status == 1 && most_viewed_products.length > 0) 
		{					
			color = settings[0].border_color;
			display_border = settings[0].display_border;
			style = settings[0].border_style;
			size = settings[0].border_size;
			products = settings[0].number_of_products;
			autoplay = settings[0].autoplay_slider;
			slider_title = settings[0].slider_title;
			slider_subtitle = settings[0].slider_subtitle;
			shop_currency = settings[0].shop_currency;				
			loop = settings[0].loop;
			product_click = settings[0].product_click;								
			length = most_viewed_products.length;									
			if(length > 0)
			{
				if (autoplay == 1) 
				{
					autoplay = true;
				} 
				else 
				{
					autoplay = false;
				}						
				if (loop == 1) 
				{
					loop = true;
				} 
				else 
				{
					loop = false;
				}
				if(display_border == 1)
				{						
					//$zestard_most_view(".quick_buy").css("border", size + " " + style + " "+ color);
					$zestard_most_view(".quick_buy").css("border", size + " " + style + " "+ color);
				}
				$zestard_most_view(".preview_title").html(slider_title);
				$zestard_most_view(".preview_subtitle").html(slider_subtitle);
								
				if(product_click == 1)
				{
					for (var i = 0; i < length; i++) 
					{							
						product_id = most_viewed_products[i].product_id;
						name = most_viewed_products[i].product_name;					
						variants = JSON.parse(most_viewed_products[i].variants);																					
						url = shop_name+'/products/'+most_viewed_products[i].product_handle;								
						//$zestard_most_view(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + most_viewed_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> <b>"+shop_currency+" "+ most_viewed_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id +"' class='product_add btn btn-info' >Add to Cart</a></li>");
						$zestard_most_view(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + most_viewed_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br><b>" + shop_currency + " " + most_viewed_products[i].product_price + "</b></li>");
						
					}	
					product_add();
				}
				else
				{							
					for (var i = 0; i < length; i++) 
					{
						product_id = most_viewed_products[i].product_id;
						name 	 = most_viewed_products[i].product_name;
						variants = JSON.parse(most_viewed_products[i].variants);
						//$zestard_most_view(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-2' data-id='"+ product_id + "' src='" + most_viewed_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-2' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + " <br> " + shop_currency + " " + most_viewed_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id  +"' class='product_add btn btn-info'>Add to Cart</a></li>");
						$zestard_most_view(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-2' data-id='"+ product_id + "' src='" + most_viewed_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-2' data-id='"+ product_id +"'>"+ name +"</a><br><b>" + shop_currency+" "+ most_viewed_products[i].product_price + "</b></li>");
					}
					show_popup();
					product_add();
					modal_product_add();
				}
				$zestard_most_view(".flexslider .slides img").css("height", "160px");
				$zestard_most_view(".flexslider .slides img").css("cursor", "pointer");
				if (length > 0) 
				{
					//$zestard_most_view('.most_viewed_products').flexslider({
					$zestard_most_view('.slides').flexslider({
						animation: "slide",
						animationLoop: loop,
						slideshow: autoplay,
						itemWidth: 410,
						itemMargin: 25,
						minItems: products,
						maxItems: products,
					});
				}
			$zestard_most_view(".flex-next").html("<img height=20 width=20 src='" + base_path_most_view + "image/right.png' />");
			$zestard_most_view(".flex-prev").html("<img height=20 width=20 src='" + base_path_most_view + "image/left.png' />");									
			$zestard_most_view(".flex-next").css("right","0");
			$zestard_most_view(".flex-next").css("position","absolute");
			$zestard_most_view(".flex-prev").css("left","0");
			$zestard_most_view(".flex-prev").css("position","absolute");
			/* 
				$zestard_most_view(".flex-next").html("<i class='glyphicon glyphicon-chevron-right'></i>");
				$zestard_most_view(".flex-prev").html("<i class='glyphicon glyphicon-chevron-left'></i>");
				$zestard_most_view(".flex-next").html("<i class='fa fa-chevron-circle-left'></i>");
				$zestard_most_view(".flex-prev").html("<i class='fa fa-chevron-circle-right'></i>"); 
				$zestard_most_view(".flex-next").html("<p><span class='glyphicon glyphicon-chevron-right'></span></p>");
				$zestard_most_view(".flex-prev").html("<p><span class='glyphicon glyphicon-chevron-left'></span></p>");					
			*/	
			}			
		} 
		else 
		{
			$zestard_most_view("#best_seller_slider").remove();
		}				
	}
	function product_add()
	{			
		$zestard_most_view(".product_add").click(function(){							
			var id = $zestard_most_view(this).attr("data-id");
			product_id = $zestard_most_view("#"+id).val();				
			if (product_id == '' )
			{
				alert('Please Select Variant');
			}
			else
			{
				$zestard_most_view.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
					location.reload();					
				});	
			}			
		});
	}
	
	function modal_product_add()
	{					
		$zestard_most_view(".product_add_modal").click(function(){							
			var id = $zestard_most_view(this).attr("data-id");
			product_id = $zestard_most_view("#select_variant_div #"+id).val();			
			if (product_id == '' )
			{
				alert('Please Select Variant');
			}
			else
			{
				$zestard_most_view.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
					location.reload();					
				});	
			}			
		});
	}
	
	function show_popup()
	{	
		var targeted_popup_class;		
		//Opening Modal
		$zestard_most_view('.product_click').on('click', function(e){				
			$zestard_most_view("#quick_buy_popup_modal").css("display","block");
			$zestard_most_view('html, body').animate({
				scrollTop: $zestard_most_view("#quick_buy_content").offset().top - 100 //+ $zestard_most_view("#quick_buy_content").height() / 2
			}, 2000);
			
			targeted_popup_class = $zestard_most_view(this).attr('data-popup-open');					
			$zestard_most_view('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);							
			e.preventDefault();
		});
		//Closing Modal
		$zestard_most_view('[data-popup-close]').on('click', function(e){
			targeted_popup_class = $zestard_most_view(this).attr('data-popup-close');			
			$zestard_most_view('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
			e.preventDefault();
		});		
		
		var id='',index;				
		$zestard_most_view(".product_click").click(function(){
			id = $zestard_most_view(this).attr("data-id");	
			length = quick_buy_products.length;				
			for (var i = 0; i < length; i++) 
			{
				if(id == quick_buy_products[i].product_id)
				{
					index = i;
				}
			}							
			price 	=	shop_currency+" "+quick_buy_products[index].product_price;
			img_src =	quick_buy_products[index].product_image;
			name 	=	quick_buy_products[index].product_name;
			desc 	=	quick_buy_products[index].product_description;
			variant_id 	=	quick_buy_products[index].product_variant_id;	
			variants = JSON.parse(quick_buy_products[index].variants);
			variants_select = "<select id='" + id + "'><option value=''>Select Variant</option>";
			$zestard_most_view.each(variants, function(key, value){
				variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
			});
			variants_select = variants_select + "</select>";	
			add_to_cart = "<a data-id='"+ id +"' class='product_add_modal btn btn-info' style='background:#7796a8;'>Add to Cart</a>";			
			$zestard_most_view("#quick_buy_image").attr("src",img_src);			
			$zestard_most_view("#product_name").html(name);
			$zestard_most_view("#product_price").html(price);
			$zestard_most_view("#product_desc").html(desc);			
			$zestard_most_view("#add_to_cart_button").html(add_to_cart);
			$zestard_most_view("#select_variant_div").html(variants_select);
			modal_product_add();
		});				
	}	