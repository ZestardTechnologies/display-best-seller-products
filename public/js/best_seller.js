var shop_name = Shopify.shop;
var base_path_best_seller = "https://zestardshop.com/shopifyapp/display_best_seller_products/public/";
var products_array = undefined, settings, parsed_data;
var settings, best_seller_products;
var jscript = document.createElement("script");
var flexslider_script = document.createElement("script");
var modal_script = document.createElement("script");
jscript.src = base_path_best_seller + "js/zestard_jquery_3.3.1.js";	
jscript.type = 'text/javascript';
jscript.async = false;		
function get_width(products)
{	
	return (window.innerWidth < 450) ? 450 : $zestard_best_seller(".best_seller").width()/products;
}
function getGridSize(products) 
{	
	return (window.innerWidth < 450) ? 1 : ((window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? products : 3);		
}
/* if(typeof jQuery == 'undefined')  */
{			
	//If Undefined or Not available, Then Load		    
	var jscript = document.createElement("script");
	jscript.src = base_path_best_seller + "js/zestard_jquery_3.3.1.js";		
	jscript.type = 'text/javascript';
	jscript.async = false;
	
	var el = document.getElementById('best_seller_slider'),
	elChild = document.createElement('div');
	elChild.id = 'jquery_div';		
	el.insertBefore(elChild, el.firstChild); 	
	
	document.getElementById('jquery_div').append(jscript);		
	jscript.onload = function(){														
	flexslider_script.src = base_path_best_seller + "js/jquery.flexslider.js";			
	flexslider_script.type = 'text/javascript';
	flexslider_script.async = false;			
	document.getElementById('best_seller_slider').append(flexslider_script);
	flexslider_script.onload = function(){					
		modal_script.src = base_path_best_seller + "js/jquery.modal.min.js";		
		modal_script.type = 'text/javascript';
		modal_script.async = false;									
		document.getElementById('best_seller_slider').append(modal_script);	
		modal_script.onload = function(){
				$zestard_best_seller = window.jQuery;
				best_seller();												
			};
		};						
	};	
}
/* else
{														
	flexslider_script.src = base_path_best_seller + "js/jquery.flexslider.js";			
	flexslider_script.type = 'text/javascript';
	flexslider_script.async = false;			
	document.getElementById('best_seller_slider').append(flexslider_script);
	flexslider_script.onload = function(){					
		modal_script.src = base_path_best_seller + "js/jquery.modal.min.js";		
		modal_script.type = 'text/javascript';
		modal_script.async = false;									
		document.getElementById('best_seller_slider').append(modal_script);	
		modal_script.onload = function() {
			$zestard_best_seller = window.jQuery;
			best_seller();											
		};
	};
} 
*/
	
function best_seller()
{					
	/* 
		$zestard_best_seller.ajax({
			url:base_path_best_seller + "get-details",
			data:{shop:Shopify.shop},
			success:function(result){			
			}
		}); 
	*/			
	$zestard_best_seller.ajax({
		url: base_path_best_seller + "get-products",
		data: {
			shop_name: shop_name
		},			
		async: false,
		success: function(result) {			
			if(result)
			{
				best_seller_products = JSON.parse(result);
			}			
		}
	}); 		
	$zestard_best_seller.ajax({
		url: base_path_best_seller + "get-settings",
		data: {
			shop_name: shop_name
		},
		async:false,
		success: function(result) {
			settings = JSON.parse(result);				
		}
	});		
	if(best_seller_products)
	{		
		if (settings[0].app_status == 1) {					
			color = settings[0].border_color;
			display_border = settings[0].display_border;
			style = settings[0].border_style;
			size = settings[0].border_size;
			products = settings[0].number_of_products;
			autoplay = settings[0].autoplay_slider;
			slider_title = settings[0].slider_title;
			slider_subtitle = settings[0].slider_subtitle;
			shop_currency = settings[0].shop_currency;				
			loop = settings[0].loop;
			product_click = settings[0].product_click;				
			length = best_seller_products.length;					
			if(length > 0)
			{
				if (autoplay == 1) 
				{
					autoplay = true;
				} 
				else 
				{
					autoplay = false;
				}						
				if (loop == 1) 
				{
					loop = true;
				} 
				else 
				{
					loop = false;
				}
				if(display_border == 1)
				{
					$zestard_best_seller(".best_seller").css("border", size + " " + style + " "+ color);
				}
				$zestard_best_seller(".preview_title").html(slider_title);
				$zestard_best_seller(".preview_subtitle").html(slider_subtitle);
								
				if(product_click == 1)
				{
					for (var i = 0; i < length; i++) 
					{
						product_id = best_seller_products[i].product_id;
						name = best_seller_products[i].product_name;					
						variants = JSON.parse(best_seller_products[i].variants);
						variants_select = "<select class='product_variants' id='" +
						product_id + "' ><option value=''>Select Variant</option>";
						$zestard_best_seller.each(variants, function(key, value){
							variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
						});
						variants_select = variants_select + "</select>";														
						
						url = shop_name+'/products/'+best_seller_products[i].product_handle;								
						$zestard_best_seller(".slides").append("<li><a href='https://"+url+"'><img style='max-width:200px !important;max-height:200px !important; margin:0 auto;' src='" + best_seller_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> <b>"+shop_currency+" "+ best_seller_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id +"' class='product_add btn btn-info' >Add to Cart</a></li>");
						//$zestard_best_seller(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + best_seller_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> </li>");
					}	
					product_add();
				}
				else
				{							
					for (var i = 0; i < length; i++) 
					{
						product_id = best_seller_products[i].product_id;
						name 	 = best_seller_products[i].product_name;
						variants = JSON.parse(best_seller_products[i].variants);
						variants_select = "<select class='product_variants' id='" +product_id + "' ><option value=''>Select Variant</option>";
						$zestard_best_seller.each(variants, function(key, value){
							variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
						});
						variants_select = variants_select + "</select>";															
						
						$zestard_best_seller(".slides").append("<li><img style='max-width:200px !important;max-height:200px !important; margin:0 auto;' class='product_click' data-popup-open='popup-5' data-id='"+ product_id + "' src='" + best_seller_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-5' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + " <br> " + shop_currency + " " + best_seller_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id  +"' class='product_add btn btn-info'>Add to Cart</a></li>");
						//$zestard_best_seller(".slides").append("<li class='best_seller_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-5' data-id='"+ product_id + "' src='" + best_seller_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-5' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + "</li>");
					}
					show_popup();
					product_add();
					modal_product_add();
				}
				$zestard_best_seller(".flexslider .slides img").css("height", "160px");
				$zestard_best_seller(".flexslider .slides img").css("cursor", "pointer");
				if (length > 0) {
					$zestard_best_seller('.best_seller').flexslider({
						animation: "slide",
						animationLoop: loop,
						slideshow: autoplay,
						itemWidth: get_width(products) - 10,						
						minItems:  getGridSize(products),
						maxItems:  getGridSize(products),
					});
				}
			$zestard_best_seller(".flex-next").html("<img height=20 width=20 src='" + base_path_best_seller + "image/right.png' />");
			$zestard_best_seller(".flex-prev").html("<img height=20 width=20 src='" + base_path_best_seller + "image/left.png' />");									
			$zestard_best_seller(".flex-next").css("right","0");
			$zestard_best_seller(".flex-next").css("position","absolute");
			$zestard_best_seller(".flex-prev").css("left","0");
			$zestard_best_seller(".flex-prev").css("position","absolute");			
			}			
		} 
		else 
		{
			$zestard_best_seller("#best_seller_slider").remove();
		}
	}
	else 
	{
		$zestard_best_seller("#best_seller_slider").remove();
	}
}
function product_add()
{			
	$zestard_best_seller(".product_add").click(function(){							
		var id = $zestard_best_seller(this).attr("data-id");
		product_id = $zestard_best_seller("#"+id).val();				
		if (product_id == '' )
		{
			alert('Please Select Variant');
		}
		else
		{				
			$zestard_best_seller.ajax({
				url: '/cart/add.js',
				data: {quantity: 1, id: product_id},
				async: false
			});
			window.location.href = '/cart';						
		}			
	});
}
	
function modal_product_add()
{					
	$zestard_best_seller(".product_add_modal").click(function(){							
		var id = $zestard_best_seller(this).attr("data-id");
		product_id = $zestard_best_seller("#select_variant_div #"+id).val();			
		if (product_id == '' )
		{
			alert('Please Select Variant');
		}
		else
		{				
			$zestard_best_seller.ajax({
				url: '/cart/add.js',
				data: {quantity: 1, id: product_id},
				async: false
			});
			window.location.href = '/cart';						
		}			
	});
}

function show_popup()
{	
	var targeted_popup_class;		
	//Opening Modal
	$zestard_best_seller('.product_click').on('click', function(e){				
		$zestard_best_seller("#best_seller_popup_modal").css("display","block");
		$zestard_best_seller('html, body').animate({
			scrollTop: $zestard_best_seller("#best_seller_content").offset().top - 100 //+ $zestard_best_seller("#best_seller_content").height() / 2
		}, 2000);
		
		targeted_popup_class = $zestard_best_seller(this).attr('data-popup-open');					
		$zestard_best_seller('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);							
		e.preventDefault();
	});
	//Closing Modal
	$zestard_best_seller('[data-popup-close]').on('click', function(e){
		targeted_popup_class = $zestard_best_seller(this).attr('data-popup-close');			
		$zestard_best_seller('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
	});		
	
	var id='',index;				
	$zestard_best_seller(".product_click").click(function(){
		id = $zestard_best_seller(this).attr("data-id");	
		length = best_seller_products.length;				
		for (var i = 0; i < length; i++) 
		{
			if(id == best_seller_products[i].product_id)
			{
				index = i;
			}
		}							
		price 	=	shop_currency+" "+best_seller_products[index].product_price;
		img_src =	best_seller_products[index].product_image;
		name 	=	best_seller_products[index].product_name;
		desc 	=	best_seller_products[index].product_description;
		variant_id 	=	best_seller_products[index].product_variant_id;	
		variants = JSON.parse(best_seller_products[index].variants);
		variants_select = "<select id='" + id + "'><option value=''>Select Variant</option>";
		$zestard_best_seller.each(variants, function(key, value){
			variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
		});
		variants_select = variants_select + "</select>";	
		add_to_cart = "<a data-id='"+ id +"' class='product_add_modal btn btn-info' style='background:#7796a8;'>Add to Cart</a>";			
		$zestard_best_seller("#best_seller_image").attr("src",img_src);			
		$zestard_best_seller("#product_name").html(name);
		$zestard_best_seller("#product_price").html(price);
		$zestard_best_seller("#product_desc").html(desc);			
		$zestard_best_seller("#add_to_cart_button").html(add_to_cart);
		$zestard_best_seller("#select_variant_div").html(variants_select);
		modal_product_add();
	});				
}
