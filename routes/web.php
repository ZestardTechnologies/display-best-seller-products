<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('preview', 'previewController@index')->middleware('cors')->name('preview');

Route::any('save-best-seller', 'BestsellerController@save_best_seller')->name('save-best-seller');

Route::any('quick-buy-products', 'BestsellerController@quick_buy_products')->name('quick-buy-products');

Route::any('get-settings', 'BestsellerController@get_settings')->middleware('cors')->name('get-settings');

Route::any('get-details', 'BestsellerController@get_details')->middleware('cors')->name('get-details');

Route::any('get-products', 'BestsellerController@get_products')->middleware('cors')->name('get-products');

Route::any('testing', 'BestsellerController@testing')->middleware('cors')->name('testing');

Route::any('products', 'BestsellerController@products')->name('products');

Route::any('save-session', 'BestsellerController@save_session')->name('save-session');

Route::any('save-products', 'BestsellerController@save_products')->name('save-products');

Route::any('save-auto-products', 'BestsellerController@save_auto_products')->name('save-auto-products');

Route::any('acknowledge', 'BestsellerController@acknowledge')->name('acknowledge');

Route::any('view-product', 'BestsellerController@save_product_count')->name('view-product');

Route::any('update-ids', 'BestsellerController@update_product_ids')->middleware('cors')->name('update-ids');



Route::get('charge-declined', function () {
    return view('charge_declined')->with('store_name', session('shop'));
})->name('charge-declined');

/* For Best Seller */
Route::any('get-seller-data', 'BestsellerController@get_bestseller_products')->middleware('cors')->name('get-seller-data');
Route::any('get-product-data', 'BestsellerController@get_product_data')->middleware('cors')->name('get-product-data');
Route::any('best-seller', 'BestsellerController@get_best_seller_view')->middleware('cors')->name('best-seller');

Route::any('track-products', 'BestsellerController@track_products')->name('track-products');
Route::any('help', 'BestsellerController@help')->name('help');

/* 
Route::get('track-products', function () {
    return view('track_products');
})->name('track-products'); 

Route::get('help', function () {
    return view('help');
})->name('help');
*/

/* For Most Viewed Products */
Route::any('get-most-product', 'BestsellerController@get_mostviewed_products')->middleware('cors')->name('get-most-product');

Route::any('update-modal-status', 'BestsellerController@update_modal_status')->middleware('cors')->name('update-modal-status');

/* For Recently Viewed Products */
Route::any('recent-product', 'BestsellerController@save_recently_viewed_product')->middleware('cors')->name('recent-product');
