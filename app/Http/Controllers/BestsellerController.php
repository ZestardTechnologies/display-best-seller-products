<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ShopModel;
use App\ProductsModel;
use App;
use DB;
use File;
use Mail;

class BestsellerController extends Controller
{
	public function get_details(Request $request)
	{
		echo $request->input('shop');	
	}
	
	public function update_modal_status(Request $request)
	{
		$shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_find->new_install = 'N';
        $shop_find->save();
	}
	
    public function save_best_seller(Request $request) 
	{       		
		$shop = session('shop');		
	    if(!empty($request->input('shop')))
        {
            $shop = $request->input('shop');
		}
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
		$select_store = DB::table('usersettings')->where('store_name', $shop)->get();      
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
		$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => 
		$select_store[0]->access_token]);
		$currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);		
		$shop_currency = $currency->shop->currency;		
		$count = DB::table('best_seller_settings')->where('store' , $shop)->count();
		
		if($request->input('status'))
		{
			$status = 1;
		}
		else
		{
			$status = 0;
		}
		if($request->input('select_products'))
		{
			$select_products = 1;
		}
		else
		{
			$select_products = 0;
		}
		if($request->input('autoplay'))
		{
			$autoplay = 1;
		}
		else
		{
			$autoplay = 0;
		}
		if($request->input('loop'))
		{
			$loop = 1;
		}
		else
		{
			$loop = 0;
		}
		if($request->input('border_status'))
		{
			$border_status = 1;
		}
		else
		{
			$border_status = 0;
		}
			
		if($count > 0)
		{		
			$settings = array(						
						'app_status' => $status,
						'slider_title' => $request->input('slider_title'),
						'slider_subtitle' => $request->input('slider_subtitle'),
						'number_of_products' => $request->input('product_no'),
						'select_products' => $select_products,
						'autoplay_slider' => $autoplay,			
						'display_border' => $border_status,
						'border_style' => $request->input('border_style'),
						'border_color' => $request->input('border_color'),
						'border_size' => $request->input('border_size'),
						'loop' => $loop,
						'product_click'=> $request->input('product_click'),						
						'new_install'	=> 'N'
						
			);
			DB::table('best_seller_settings')->where('store' , $shop)->update($settings);
		}
		else
		{
			$settings = array(
						'store' => $shop,
						'app_status' => $status,
						'slider_title' => $request->input('slider_title'),
						'slider_subtitle' => $request->input('slider_subtitle'),
						'number_of_products' => $request->input('product_no'),
						'select_products' => $select_products,
						'autoplay_slider' => $autoplay,
						'display_border' => $border_status,
						'border_style' => $request->input('border_style'),
						'border_color' => $request->input('border_color'),
						'border_size' => $request->input('border_size'),
						'loop' => $loop,
						'product_click'=> $request->input('product_click'),						
						'shop_currency'=>$shop_currency,
						'new_install'	=> 'N'
			);
			DB::table('best_seller_settings')->insert($settings);			
		}
		$notification = array(
		'message' => 'Settings Saved Successfully.',
		'alert-type' => 'success'); 
		return redirect()->route('dashboard', ['shop' => $shop, 'notification' => $notification])->with('notification',$notification);	
    }	
	
	public function acknowledge(Request $request)
	{		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$shop = session('shop');
		$error = $request->input('response');
		$message = "Following Analytics error generated in store " . $shop . ":\n" . $error."\n";
		mail("girish.zestard@gmail.com", "Regarding Analytics Error", $message);
	}
	
	public function get_best_seller_view(Request $request)
	{
		$shop_name = $request->shop_name;        
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
		if($shop_find->status == "active")
        {
            return view('best_seller_view');
        }
        else 
		{
            return "Page Not Found";
        }
	}
	public function get_settings(Request $request)
	{
		$shop = $request->input('shop_name');		
		$settings = DB::table('best_seller_settings')->where('store' , $shop)->get();
		return json_encode($settings);		
	}
	
	public function save_auto_products(Request $request)
	{					
		$app_settings = DB::table('appsettings')->where('id', 1)->first();		
        $shop = session('shop');
		$select_store = DB::table('usersettings')->where('store_name', $shop)->get();      
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>		
		$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => 
		$select_store[0]->access_token]);		
		$products = array_count_values($request->input('products'));		
		arsort($products);
		$product_ids = array();
		foreach($products as $key=>$value)
		{
			array_push($product_ids, $key);						
		}	
		
		if(count($product_ids) < 6)
		{
			$ids = implode(",",$product_ids);
		}
		else
		{
			$ids = implode(",",array_slice($product_ids,0,6));
		}
		$variants = array();
		DB::table('best_seller_products')->where("store", $shop)->delete();
		$shopify_products = $sh->call(['URL' => '/admin/products.json?ids='.$ids,'METHOD' => 'GET']);														
		$i=0;							
					
		foreach($shopify_products as $product)
		{		
			foreach($product as $attributes)
			{					
				$variant_id = $attributes->variants[0]->id;						
				foreach($attributes->variants as $variant)			
				{
					$variants[$variant->id] = $variant->title;
				}											
				if(empty($attributes->images))
				{
					$img_src = 'https://zestardshop.com/shopifyapp/best_seller/public/image/no_image.png';
				}
				else
				{
					$img_src =$attributes->images[0]->src;
				}											
				$data	= array(
							'store'			=> $shop,
							'product_id' 	=> "$attributes->id",
							/* 'variants'		=> json_encode($variants),
							'product_variant_id' => "$variant_id",
							'product_name'  => $attributes->title,
							'product_price' => $attributes->variants[0]->price,
							'product_image' => $img_src,
							'product_handle' => $attributes->handle,
							'product_description' =>  $attributes->body_html */
							 );						
				DB::table('best_seller_products')->insert($data);
				$i++;	
			}
		}		
	}
		
	public function get_product_data(Request $request)
	{	
		$product_data = array();
		$app_settings = DB::table('appsettings')->where('id', 1)->first();		
        $shop = session('shop');
		$select_store = DB::table('usersettings')->where('store_name', $shop)->get();      
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>		
		$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => 
		$select_store[0]->access_token]);		
		$products = array_count_values($request->input('products'));		
		arsort($products);
		$product_ids = array();		
		foreach($products as $key=>$value)
		{
			array_push($product_ids, $key);						
		}			
		if(count($product_ids) < 6)
		{
			$ids = implode(",",$product_ids);
		}
		else
		{
			$ids = implode(",",array_slice($product_ids,0,6));
		}
		$variants = array();
		DB::table('best_seller_products')->where("store", $shop)->delete();
		$shopify_products = $sh->call(['URL' => '/admin/products.json?ids='.$ids,'METHOD' => 'GET']);														
		$i=0;												
		foreach($shopify_products as $product)
		{		
			foreach($product as $attributes)
			{																			
				$data = array(																								
								'product_name'  => $attributes->title,								
								'quantity_sold'		=> $products[$attributes->id]
							);											
				array_push($product_data, $data);			
			}
		}
		echo json_encode($product_data);
	}
	
	/* For Recently Viewed Products */
	public function save_recently_viewed_product(Request $request)
	{		
		$product_ids = $request->input('product_ids');
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get(); 
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);		
		$products = $sh->call(['URL' => '/admin/products.json?ids='.$product_ids,'METHOD' => 'GET']);														
		$recent_products = array();	
		foreach($products as $product)
		{		
			foreach($product as $attributes)
			{	
				
				$variant_id = $attributes->variants[0]->id;						
				foreach($attributes->variants as $variant)			
				{
					$variants[$variant->id] = $variant->title;
				}											
				if( empty($attributes->images))
				{
					$img_src = 'https://zestardshop.com/shopifyapp/best_seller/public/image/no_image.png';
				}
				else
				{
					$img_src =$attributes->images[0]->src;
				}											
				$data	= array(
								'store'			=> $shop,
								'product_id' 	=> $attributes->id,
								'variants'		=> json_encode($variants),
								'product_variant_id' => "$variant_id",
								'product_name'  => $attributes->title,
								'product_price' => $attributes->variants[0]->price,
								'product_image' => $img_src,
								'product_handle' => $attributes->handle,
								'product_description' =>  $attributes->body_html
							);
				array_push($recent_products, $data);				
			}
		}							
		$product_data = array();
		$expiry = $request->input('expiry');	
		$product_data['expire_timestamp'] 	= $expiry;
		$product_data['product_data'] 		= $recent_products;
		return json_encode($product_data);
		//$product = DB::table("recently_viewed_products")->where('shop_name',$shop_name)->get();
	}
	
	public function get_products(Request $request)
	{		
		$shop = $request->input('shop_name');		
		$app_settings = DB::table('appsettings')->where('id', 1)->first();        	
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get(); 
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);		
		$i=0;			
		$product_ids = ProductsModel::select('product_id')->where('store' , $shop)->get()->toArray();
		$ids_array = array();		
		foreach($product_ids as $id)
		{
			array_push($ids_array, $id['product_id']);
		}		
		$products = $sh->call(['URL' => '/admin/products.json?ids='.implode(",",$ids_array)."&published_status=published",'METHOD' => 'GET']);														
		/* echo "<pre>";
		foreach($products as $product)
		{
			foreach($product as $attribute)
			{
				foreach($attribute->variants as $variant)
				{	
					
				}
			}
			
		} */		
		$best_seller_products = array();		
		//$updated_ids = explode(",",$ids);
		foreach($products as $product)
		{		
			foreach($product as $attributes)
			{	
				$variants = array();
				$variant_id = $attributes->variants[0]->id;						
				foreach($attributes->variants as $variant)			
				{
					if($variant->inventory_quantity > 0)
					{
						$variants[$variant->id] = $variant->title;
					}
					else if($variant->inventory_policy == "continue")
					{
						$variants[$variant->id] = $variant->title;
					}
				}											
				if(empty($attributes->images))
				{
					$img_src = 'https://zestardshop.com/shopifyapp/quick_buy/public/image/no_image.png';
				}
				else
				{
					$img_src =$attributes->images[0]->src;
				}	
				if(!empty($variants))
				{					
					$data = array(
							'store'			=> $shop,
							'product_id' 	=> $ids_array[$i],
							'variants'		=> json_encode($variants),
							'product_variant_id' => "$variant_id",
							'product_name'  => $attributes->title,
							'product_price' => $attributes->variants[0]->price,
							'product_image' => $img_src,
							'product_handle' => $attributes->handle,
							'product_description' =>  $attributes->body_html
						);	
					array_push($best_seller_products,$data);			
					$i++;
				}				
			}
		}							
		return json_encode($best_seller_products);	
	}
	
	public function track_products(Request $request)
	{
		$shop = session('shop');				
	    if(!empty($_GET['shop']))
        {
            $shop = $_GET['shop'];
		}		
		return view('track_products', ['shop' => $shop]);
	}
	
	public function help(Request $request)
	{
		$shop = session('shop');		
	    if(!empty($_GET['shop']))
        {
            $shop = $_GET['shop'];
		}		
		return view('help', ['shop' => $shop]);
	}
	
	/* For Best Seller */	
	public function save_session(Request $request)
	{		
		echo session('ids');
		echo $request->input('id');
		if($request->input('flag') == 1)
		{
			if(empty(session('ids')))
			{
				session(['ids' => $request->input('id')]);
			}
			else
			{
				$ids=session('ids').",".$request->input('id');
				session(['ids' => $ids ]);				
			}
		}
		else
		{
			if(empty(session('ids')))
			{
				
			}
			else
			{
				$ids=session('ids');
				$array_ids=explode(",",$ids);
				$key = array_search($request->input('id'), $array_ids);
				$key;
				unset($array_ids[$key]);				
				session(['ids' => implode(",",$array_ids)]);				
			}
		}
	}
	
	public function update_product_ids(Request $request)
	{
		$updated_product_ids= $request->input('product_ids');				
		session(['updated_product_ids' => implode(",", $updated_product_ids)]);
		echo session('updated_product_ids');
	}
	
	public function testing()
	{
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get(); 
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);		
		dd($sh->call(['URL' => '/admin/smart_collections.json','METHOD' => 'GET']));		
		
	}
	
}
