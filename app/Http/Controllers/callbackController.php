<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\ProductsModel;
use File;
use Mail;

class callbackController extends Controller
{
    public function index(Request $request)
    {
        $sh = App::make('ShopifyAPI');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if(!empty($_GET['shop']))
        {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            if(count($select_store) > 0)
            {                                
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/'. $id .'.json';
                $charge = $sh->call(['URL' => $url,'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;                
                if(!empty($charge_id) && $charge_id > 0 && $charge_status == "active")
                {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard',['shop' => $shop]);
                }
                else{
                    return redirect()->route('payment_process');
                }
            }
            else{
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);

               $permission_url = $sh->installURL(['permissions' => array('read_themes', 'write_themes','read_products', 'read_analytics'), 'redirect' => $app_settings->redirect_url]);
               return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request)
    {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
		
        if(!empty($request->input('shop')) && !empty($request->input('code')))
        {
			$shop = $request->input('shop'); //shop name

			$select_store = DB::table('usersettings')->where('store_name', $shop)->get();
			if(count($select_store) > 0)
			{               
				//Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges'. $id .'.json';
                $charge = $sh->call(['URL' => $url,'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if(!empty($charge_id) && $charge_id > 0 && $charge_status == "active")
                {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard');
                }
                else
				{
                     return redirect()->route('payment_process');
                }
           }
			$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try
            {
                $verify = $sh->verifyRequest($request->all());
                if ($verify)
                {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);
                    
                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' =>""]);
                    $shop_find = ShopModel::where('store_name' , $shop)->first();
                    $shop_id = $shop_find->id;

                    $best_seller_encrypt = crypt($shop_id,"ze");
                    $finaly_encrypt = str_replace(['/','.'], "Z", $best_seller_encrypt);

                    $update_encrypt = ShopModel::where('id',$shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                   //for creating app uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url').'/uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);
                    					
                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                    foreach($theme->themes as $themeData){
                        if($themeData->role == 'main'){

                            $snippets_arguments = ['id' => $finaly_encrypt];
                            $theme_id = $themeData->id;
                            $view = (string)View('snippets',$snippets_arguments);
                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/best-seller.liquid', 'value' => $view] ] ]);
                        }
                    }
					
                    session(['shop' => $shop]);
					
					//creating the Recuring charge for app
					$url = 'https://' . $shop . '/admin/recurring_application_charges.json';					
					//if(stristr($shop, "zestard") && 1==2)                    				
					if($shop == "free-theme-test.myshopify.com")						
                    {
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array (
                                'recurring_application_charge' => array (
                                'name' => 'Display Best Seller Products',
                                'price' => 0.01,
                                'return_url' => url('payment_success'),
                                'test' => true
                                )
                            )
                        ], false);
                    }
                    else
					{
						$charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array (
                                'recurring_application_charge' => array (
                                'name' => 'Display Best Seller Products',
                                'price' => 4.99,
                                'return_url' => url('payment_success'),
                                //'test' => true
                                )
                            )
                        ], false);
                        /* $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array (
                                'recurring_application_charge' => array (
                                'name' => 'Display Best Seller Products',
                                'price' => 4.99,
                                'return_url' => url('payment_success'),                                
                                //'test' => true
                                )
                            )
                        ], false); */
                    }					
                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string)$charge->recurring_application_charge->id , 'api_client_id' =>$charge->recurring_application_charge->api_client_id , 'price' =>$charge->recurring_application_charge->price , 'status' =>$charge->recurring_application_charge->status , 'billing_on' =>$charge->recurring_application_charge->billing_on , 'payment_created_at' =>$charge->recurring_application_charge->created_at , 'activated_on' =>$charge->recurring_application_charge->activated_on , 'trial_ends_on' =>$charge->recurring_application_charge->trial_ends_on , 'cancelled_on' =>$charge->recurring_application_charge->cancelled_on , 'trial_days' =>$charge->recurring_application_charge->trial_days , 'decorated_return_url' =>$charge->recurring_application_charge->decorated_return_url , 'confirmation_url' =>$charge->recurring_application_charge->confirmation_url , 'domain' =>$shop ]);
                    $shop_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
					
					$headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $msg = '<table>
						<tr>
							<th>Shop Name</th>
							<td>'.$shop_info->shop->name.'</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>'.$shop_info->shop->email.'</td>
						</tr>
						<tr>
							<th>Domain</th>
							<td>'.$shop_info->shop->domain.'</td>
						</tr>
						<tr>
							<th>Phone</th>
							<td>'.$shop_info->shop->phone.'</td>
						</tr>
						<tr>
							<th>Shop Owner</th>
							<td>'.$shop_info->shop->shop_owner.'</td>
						</tr>
						<tr>
							<th>Country</th>
							<td>'.$shop_info->shop->country_name.'</td>
						</tr>
						<tr>
							<th>Plan</th>
							<td>'.$shop_info->shop->plan_name.'</td>
						</tr>
					  </table>';

					mail("support@zestard.com","Display Best Seller Products App Installed",$msg,$headers);                                        
					
					
                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="'.$charge->recurring_application_charge->confirmation_url.'"</script>';                                     
                }
                else
                {
                    // Issue with data
                }

            }
            catch (Exception $e)
            {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard()
    {
        $shop = session('shop');		
		/* dd(session('notification')); */
        if(!empty($_GET['shop']))
        {
            $shop = $_GET['shop'];
		}
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name' , $shop)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);                                             
            
        $access_token = $shop_find->access_token;
        		
        $settings = DB::table('best_seller_settings')->where('store' , $shop)->get();   
		$new_install = $shop_find->new_install;
		$currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);		
		$country_currency = $currency->shop->currency;
		
		$product_ids = ProductsModel::select('product_id')->where('store' , $shop)->get()->toArray();
		$best_seller_products = array();
		if(count($product_ids) > 0)
		{
			$ids_array = array();		
			foreach($product_ids as $id)
			{
				array_push($ids_array, $id['product_id']);
			}		
			$products = $sh->call(['URL' => '/admin/products.json?ids='.implode(",",$ids_array)."&published_status=published",'METHOD' => 'GET']);																
					
			$i = 0;		
			//$updated_ids = explode(",",$ids);
			foreach($products as $product)
			{		
				foreach($product as $attributes)
				{	
					$variants = array();
					$variant_id = $attributes->variants[0]->id;						
					foreach($attributes->variants as $variant)			
					{
						$variants[$variant->id] = $variant->title;						
						/* if($variant->inventory_quantity > 0)
						{
							$variants[$variant->id] = $variant->title;						
						}
						else if($variant->inventory_policy == "continue")
						{
							$variants[$variant->id] = $variant->title;						
						} */					
					}											
					if(empty($attributes->images))
					{
						$img_src = 'https://zestardshop.com/shopifyapp/quick_buy/public/image/no_image.png';
					}
					else
					{
						$img_src =$attributes->images[0]->src;
					}	
					if(!empty($variants))
					{					
						$data = array(
								'store'			=> $shop,
								'product_id' 	=> $ids_array[$i],
								'variants'		=> json_encode($variants),
								'product_variant_id' => "$variant_id",
								'product_name'  => $attributes->title,
								'product_price' => $attributes->variants[0]->price,
								'product_image' => $img_src,
								'product_handle' => $attributes->handle,
								'product_description' =>  $attributes->body_html
							);	
						array_push($best_seller_products,$data);			
						$i++;
					}				
				}
			}		
		}
		return view('dashboard', array('shopdomain'=> $shop_find->store_name,'best_seller_products' =>$best_seller_products,  'settings' => $settings, 'currency' => $country_currency, 'new_install' => $new_install));
    }

    public function payment_method(Request $request)
    {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        if(count($select_store) > 0)
        {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/'. $charge_id .'.json';
            $charge = $sh->call(['URL' => $url,'METHOD' => 'GET']);
            if(count($charge) > 0)
            {
                if($charge->recurring_application_charge->status == "pending")
                {
                    echo '<script>window.top.location.href="'.$charge->recurring_application_charge->confirmation_url.'"</script>';
                }
                elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired" ) {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array (
                            'recurring_application_charge' => array (
                            'name' => 'Display Best Seller Products',
                            'price' => 4.99,
                            'return_url' => url('payment_success'),
                            'test' => true
                            )
                        )
                    ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string)$charge->recurring_application_charge->id , 'api_client_id' =>$charge->recurring_application_charge->api_client_id , 'price' =>$charge->recurring_application_charge->price , 'status' =>$charge->recurring_application_charge->status , 'billing_on' =>$charge->recurring_application_charge->billing_on , 'payment_created_at' =>$charge->recurring_application_charge->created_at , 'activated_on' =>$charge->recurring_application_charge->activated_on , 'trial_ends_on' =>$charge->recurring_application_charge->trial_ends_on , 'cancelled_on' =>$charge->recurring_application_charge->cancelled_on , 'trial_days' =>$charge->recurring_application_charge->trial_days , 'decorated_return_url' =>$charge->recurring_application_charge->decorated_return_url , 'confirmation_url' =>$charge->recurring_application_charge->confirmation_url , 'domain' =>$shop ]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="'.$charge->recurring_application_charge->confirmation_url.'"</script>';

                }
                elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/'. $charge_id .'/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url,'METHOD' => 'POST','HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('dashboard');                    
                }
            }
        }
    }

    public function payment_compelete(Request $request)
    {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $charge_id = $_GET['charge_id'];
                $url = 'admin/recurring_application_charges/#{'. $charge_id .'}.json';
                $charge = $sh->call(['URL' => $url,'METHOD' => 'GET',]);
                $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if($status == "accepted")
        {
             $active_url = '/admin/recurring_application_charges/'. $charge_id .'/activate.json';
             $Activate_charge = $sh->call(['URL' => $active_url,'METHOD' => 'POST','HEADERS' => array('Content-Length: 0')]);
             $Activatecharge_array = get_object_vars($Activate_charge);
             $active_status = $Activatecharge_array['recurring_application_charge']->status;
             $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);            
            return redirect()->route('dashboard');

        }
		elseif ($status == "declined") 
		{
			return redirect()->route('charge-declined');
            //echo '<script>window.top.location.href="https://'.$shop.'/admin/apps"</script>';
        }
    }
}
