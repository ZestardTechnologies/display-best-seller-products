<div class="col-sm-12 form-group text-center" id="quick_buy_slider">
	<br>
	<h1 class="preview_title">
	</h1>
	<h4 class="preview_subtitle">
	</h4>						
	<div class="quick_buy flexslider carousel"> 
		<ul class='slides best_seller_products'>		
		</ul>
	</div>
</div>
<div class="popup" id="best_seller_popup_modal" data-popup="popup-3" style="width:100%;z-index:999999999999999;display:none;">
	<div class="popup-inner" id="best_seller_content" style="z-index:999999999999999;height:500px">  
   		<div class="container">
			<div class="col-md-6" style="float:left;width:28%">
				<img id="best_seller_image" style="height:320px;width:310px" class='product_click' src=''/>				
          	</div>
			<div id="product_content" class="col-md-6" style="float:right;width:72%">
				<h2 id="product_name"></h2>              	
				<h4 id="product_price" style="float:left;width:50%"></h4>
                <div id="select_variant_div" style="float:left;width:50%"></div> 
				<br><br>
				<div class="text-left" id="add_to_cart_button"></div>
              	<br>
                <h5 id="product_desc"></h5>
			</div>
		</div>    
    	<!--p class="text-right"><a data-popup-close="popup-3" href="#">Close</a></p-->
    <a class="popup-close" data-popup-close="popup-3" href="#">x</a>
	</div>
</div>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/flexslider.css"></script>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/mdb/font-awesome.min.css"></script>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/custom_modal.css"></script>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/bootstrap.min.css"></script>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/style.css"></script>
<link href="https://zestardshop.com/shopifyapp/best_seller/public/css/jquery.modal.min.css"></script>
<link rel="stylesheet" href="https://zestardshop.com/shopifyapp/best_seller/public/css/jquery.modal.min.css" />
<script src="https://zestardshop.com/shopifyapp/best_seller/public/js/jquery.modal.min.js"></script>

<script>

ShopifyApp.init({
	apiKey: 'd25ca9d753b82af42ec764cb74380f63',
	shopOrigin: 'https://' + shop_name,
	forceRedirect: false,
	debug:true
}); 
product_ids_array = new Array();
var shopifyQL = "SHOW product_id FROM sales UNTIL today ORDER BY product_id ASC";
var renderData = function(response){														
	length = response.result['data'].length;				
	alert(length);
	for(i = 0; i<length; i++)
	{				
		if(response.result['data'][i]['0'] == 0)
		{
			
		}
		else
		{
			product_ids_array.push(response.result['data'][i]['0']);
		}
	}
	console.log(product_ids_array);	
};

var handleError = function(response){							
};
ShopifyApp.Analytics.fetch({
	query: shopifyQL,
	success: renderData,
	error: handleError
});
best_seller();
function best_seller()
{		
	/* $zestard_best_seller.ajax({
		url: base_path_best_seller + "get-seller-data",
		data: {
			shop_name: shop_name
		},
		crossDomain: true,
		async:false,	
		success: function(result) {
			settings = JSON.parse(result);				
		}
	}); */		
	$zestard_best_seller.ajax({
		url:base_path_best_seller + "get-details",
		data:{shop:Shopify.shop},
		success:function(result){			
		}
	});
	
    /* $zestard_best_seller.ajax({
        url: base_path_best_seller + "get-products",
        data: {
            shop_name: shop_name
        },
        crossDomain: true,
        async: false,
        success: function(result) {			
			if(result)
			{
				quick_buy_products = JSON.parse(result);
			}			
        }
    }); */
	/* if(quick_buy_products)
	{
		$zestard_best_seller.ajax({
			url: base_path_best_seller + "get-settings",
			data: {
				shop_name: shop_name
			},
			crossDomain: true,
			success: function(result) {
				settings = JSON.parse(result);
				if (settings[0].app_status == 1) {					
					color = settings[0].border_color;
					display_border = settings[0].display_border;
					style = settings[0].border_style;
					size = settings[0].border_size;
					products = settings[0].number_of_products;
					autoplay = settings[0].autoplay_slider;
					slider_title = settings[0].slider_title;
					slider_subtitle = settings[0].slider_subtitle;
					shop_currency = settings[0].shop_currency;				
					loop = settings[0].loop;
					product_click = settings[0].product_click;				
					length = quick_buy_products.length;					
					if(length > 0)
					{
						if (autoplay == 1) 
						{
							autoplay = true;
						} 
						else 
						{
							autoplay = false;
						}						
						if (loop == 1) 
						{
							loop = true;
						} 
						else 
						{
							loop = false;
						}
						if(display_border == 1)
						{
							$zestard_best_seller(".quick_buy").css("border", size + " " + style + " "+ color);
						}
						$zestard_best_seller(".preview_title").html(slider_title);
						$zestard_best_seller(".preview_subtitle").html(slider_subtitle);
										
						if(product_click == 1)
						{
							for (var i = 0; i < length; i++) 
							{
								product_id = quick_buy_products[i].product_id;
								name = quick_buy_products[i].product_name;					
								variants = JSON.parse(quick_buy_products[i].variants);
								variants_select = "<select class='product_variants' id='" +
								product_id + "' ><option value=''>Select Variant</option>";
								$zestard_best_seller.each(variants, function(key, value){
									variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
								});
								variants_select = variants_select + "</select>";
																
								
								url = shop_name+'/products/'+quick_buy_products[i].product_handle;								
								$zestard_best_seller(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + quick_buy_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> <b>"+shop_currency+" "+ quick_buy_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id +"' class='product_add btn btn-info' >Add to Cart</a></li>");
								//$zestard_best_seller(".slides").append("<li style='height:50%;width:50%'><a href='https://"+url+"'><img height=160 width=160 src='" + quick_buy_products[i].product_image + "'/></a><p><a href='https://"+url+"'>"+ name +"</a><br> </li>");
							}	
							product_add();
						}
						else
						{							
							for (var i = 0; i < length; i++) 
							{
								product_id = quick_buy_products[i].product_id;
								name 	 = quick_buy_products[i].product_name;
								variants = JSON.parse(quick_buy_products[i].variants);
								variants_select = "<select class='product_variants' id='" +product_id + "' ><option value=''>Select Variant</option>";
								$zestard_best_seller.each(variants, function(key, value){
									variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
								});
								variants_select = variants_select + "</select>";															
								
								$zestard_best_seller(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-3' data-id='"+ product_id + "' src='" + quick_buy_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-3' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + " <br> " + shop_currency + " " + quick_buy_products[i].product_price +"</b><p><label>&nbsp;</label><a data-id='"+ product_id  +"' class='product_add btn btn-info'>Add to Cart</a></li>");
								//$zestard_best_seller(".slides").append("<li class='quick_products' style='height:50%;width:50%'><img width=160 height=160  class='product_click' data-popup-open='popup-3' data-id='"+ product_id + "' src='" + quick_buy_products[i].product_image + "'/></a><p><a class='product_click' data-popup-open='popup-3' data-id='"+ product_id +"'>"+ name +"</a><br> <b>"+variants_select + "</li>");
							}
							show_popup();
							product_add();
							modal_product_add();
						}
						$zestard_best_seller(".flexslider .slides img").css("height", "160px");
						$zestard_best_seller(".flexslider .slides img").css("cursor", "pointer");
						if (length > 0) {
							$zestard_best_seller('.quick_buy').flexslider({
								animation: "slide",
								animationLoop: loop,
								slideshow: autoplay,
								itemWidth: 410,
								itemMargin: 25,
								minItems: products,
								maxItems: products,
							});
						}
					$zestard_best_seller(".flex-next").html("<img height=20 width=20 src='" + base_path_best_seller + "image/right.png' />");
					$zestard_best_seller(".flex-prev").html("<img height=20 width=20 src='" + base_path_best_seller + "image/left.png' />");									
					$zestard_best_seller(".flex-next").css("right","0");
					$zestard_best_seller(".flex-next").css("position","absolute");
					$zestard_best_seller(".flex-prev").css("left","0");
					$zestard_best_seller(".flex-prev").css("position","absolute");
					
					}			
				} 
				else 
				{
					$zestard_best_seller("#best_seller_slider").remove();
				}
			}
		});
	} */
}
	function product_add()
	{			
		$zestard_best_seller(".product_add").click(function(){							
			var id = $zestard_best_seller(this).attr("data-id");
			product_id = $zestard_best_seller("#"+id).val();				
			if (product_id == '' )
			{
				alert('Please Select Variant');
			}
			else
			{
				$zestard_best_seller.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
					location.reload();					
				});	
			}			
		});
	}
	
	function modal_product_add()
	{					
		$zestard_best_seller(".product_add_modal").click(function(){							
			var id = $zestard_best_seller(this).attr("data-id");
			product_id = $zestard_best_seller("#select_variant_div #"+id).val();			
			if (product_id == '' )
			{
				alert('Please Select Variant');
			}
			else
			{
				$zestard_best_seller.post('/cart/add.js', 'quantity=' + 1 + '&id=' + product_id,function(){							
					location.reload();					
				});	
			}			
		});
	}
	
	function show_popup()
	{	
		var targeted_popup_class;		
		//Opening Modal
		$zestard_best_seller('.product_click').on('click', function(e){				
			$zestard_best_seller("#best_seller_popup_modal").css("display","block");
			$zestard_best_seller('html, body').animate({
				scrollTop: $zestard_best_seller("#best_seller_content").offset().top - 100 //+ $zestard_best_seller("#best_seller_content").height() / 2
			}, 2000);
			
			targeted_popup_class = $zestard_best_seller(this).attr('data-popup-open');					
			$zestard_best_seller('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);							
			e.preventDefault();
		});
		//Closing Modal
		$zestard_best_seller('[data-popup-close]').on('click', function(e){
			targeted_popup_class = $zestard_best_seller(this).attr('data-popup-close');			
			$zestard_best_seller('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
			e.preventDefault();
		});		
		
		var id='',index;				
		$zestard_best_seller(".product_click").click(function(){
			id = $zestard_best_seller(this).attr("data-id");	
			length = quick_buy_products.length;				
			for (var i = 0; i < length; i++) 
			{
				if(id == quick_buy_products[i].product_id)
				{
					index = i;
				}
			}							
			price 	=	shop_currency+" "+quick_buy_products[index].product_price;
			img_src =	quick_buy_products[index].product_image;
			name 	=	quick_buy_products[index].product_name;
			desc 	=	quick_buy_products[index].product_description;
			variant_id 	=	quick_buy_products[index].product_variant_id;	
			variants = JSON.parse(quick_buy_products[index].variants);
			variants_select = "<select id='" + id + "'><option value=''>Select Variant</option>";
			$zestard_best_seller.each(variants, function(key, value){
				variants_select = variants_select + "<option value='"+ key +"'>" + value + "</option>";	
			});
			variants_select = variants_select + "</select>";	
			add_to_cart = "<a data-id='"+ id +"' class='product_add_modal btn btn-info' style='background:#7796a8;'>Add to Cart</a>";			
			$zestard_best_seller("#best_seller_image").attr("src",img_src);			
			$zestard_best_seller("#product_name").html(name);
			$zestard_best_seller("#product_price").html(price);
			$zestard_best_seller("#product_desc").html(desc);			
			$zestard_best_seller("#add_to_cart_button").html(add_to_cart);
			$zestard_best_seller("#select_variant_div").html(variants_select);
			modal_product_add();
		});				
	}
</script>
